<?php

/**
 * @file
 * template.php
 */

/**
 * Add bootstrap responsive class to images.
 */
function epscor_bootstrap_preprocess_image_style(&$vars) {
  // Can be 'img-responsive, img-rounded', 'img-circle', or 'img-thumbnail'.
  $vars['attributes']['class'][] = 'img-responsive';
}

/**
 * Implements hook_menu_link()..
 */
function epscor_bootstrap_menu_link(array $variables) {
  $element = $variables['element'];
  $sub_menu = '';

  if ($element['#below']) {
    unset($element['#below']['#theme_wrappers']);
    $sub_menu = '<ul class="lower-level">' . drupal_render($element['#below']) . '</ul>';
    // Generate as standard dropdown.
    $element['#title'] .= ' <span class="caret"></span>';
    if ($variables['element']['#theme'] != 'menu_link__ercore_admin_menu') {
      $element['#attributes']['class'][] = 'dropdown';
    }
    $element['#localized_options']['html'] = TRUE;

    // Set dropdown trigger element to # to prevent inadvertant page loading
    // when a submenu link is clicked.
    //    $element['#localized_options']['attributes']['data-target'] = '#';
    //     $element['#localized_options']['attributes']['class'][] = 'dropdown-toggle';
//            $element['#localized_options']['attributes']['data-toggle'] = 'dropdown';
  }
  // On primary navigation menu, class 'active' is not set on active menu item.
  // @see https://drupal.org/node/1896674
  if (($element['#href'] == $_GET['q'] || ($element['#href'] == '<front>' && drupal_is_front_page())) && (empty($element['#localized_options']['language']))) {
    $element['#attributes']['class'][] = 'active';
  }
  $output = l($element['#title'], $element['#href'], $element['#localized_options']);
  return '<li' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . "</li>\n";
}
